import React from "react";
import { StyleSheet, View, TextInput, Button } from "react-native";

export default class SearchBar extends React.Component {
  constructor() {
    super();

    this.state = {
      text: ""
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <TextInput
          style={styles.input}
          placeholder="Search for something"
          value={this.state.text}
          onChangeText={this._onChange}
        />
        <Button title="🕵️‍" onPress={this._submitSearch} />
      </View>
    );
  }

  _onChange = text => {
    this.setState({ text });
  };

  _submitSearch = e => {
    this.props.onSearch(this.state.text);
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 0,
    flexDirection: "row",
    justifyContent: "flex-start"
  },
  input: {
    height: 20,
    flex: 1,
    marginLeft: 20,
    marginRight: 20
  }
});
