import React from "react";
import { StyleSheet, Text, View, FlatList, Image } from "react-native";
import SearchBar from "./Components/SearchBar";

export default class App extends React.Component {
  constructor() {
    super();

    this.state = {
      results: []
    };
  }
  render() {
    return (
      <View style={styles.container}>
        <SearchBar onSearch={this._searchReddit} />
        <FlatList
          data={this.state.results}
          renderItem={({ item }) => (
            <Image
              source={{ uri: item.data.thumbnail }}
              style={{ width: 200, height: 200, margin: 10 }}
            />
          )}
        />
      </View>
    );
  }

  _searchReddit = term => {
    const url = `https://www.reddit.com/r/aww/search.json?q=${term}&type=link&restrict_sr=true&sort=top&t=all`;

    fetch(url)
      .then(results => {
        return results.json();
      })
      .then(json => {
        this.setState({ results: json.data.children });
      })
      .catch(err => {
        console.log("got error", err);
      });
  };
}

const styles = StyleSheet.create({
  container: {
    marginTop: 40,
    marginLeft: 20,
    marginRight: 20,
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center"
  }
});
